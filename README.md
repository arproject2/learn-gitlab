# 行動裝置遊戲設計期末專案

### 組員:
`108AC1021 謝妤羚`
`108AC2018 陳冠廷`
`108AC2023 温芷瓔`
`110AEK002 湯曉琪`
`110AEK003 李鴻杰`

# 
### 主題：
##### Krabby Patty
AR多人連線漢堡協作遊戲

# 

### 遊玩人數/時間：
3-5人，遊戲時長約3min

# 

### 機制說明：
1.環境：掃描辨識圖卡，啟動AR環境，並顯示漢堡菜單(漢堡組合順序要求)。
2.玩家：每位玩家會隨機分配到一樣食材
點擊”疊”按鍵將食材從袋子內拖曳出並拋往中央的碟子，食材將由玩家拋出順序依序堆疊。
3.判定：堆疊順序錯誤則製作失敗、該菜單將廢除、並換下一張菜單。
4.目標：成功菜單將計分、挑戰在倒數時限內成功製作出越多漢堡菜單!

# 

### Unity平台  
##### Assets Requset
* Phton PUN2
* Photon Networking Classic
(進行多人連線與資料傳遞)
##### Vuforia Engine
* 使用ARCamera掃描Target顯示模型實現AR功能


# 遊玩步驟
### Step1、LogginScene
使用Photon PUN2進行多人連線
Multipleplayer CustomMatchmaking
1. 玩家輸入使用名稱 (輸入名稱會保留至default 再次開啟遊戲時也會顯示)
2. 登入後進入Lobby頁，輸入房間名稱、房間最大人數即創建，並將房間保留至上方可視列表處
3. 房間創建者：進入房間時上方列表改為顯示房間內人員
尚在Lobby頁的玩家將會在上方列表看見已存在的房間名稱
4. 房間創建者點擊開始遊戲，進入遊戲場景
![GITHUB]( https://sandratong.com/1.png "加入房間" )

### Step2、GameScene
1. 進入遊戲後掃描Krabby Patty圖卡，顯示托盤與漢堡包底部
![GITHUB]( https://sandratong.com/2.png "掃描Krabby Patty圖卡" )
2. 右上方顯示MENU，顯示菜單要求
3. 左下方顯示該玩家現有食材，若有符合菜單指定要求，則按下”疊”，畫面中生成該食材模型
![GITHUB]( https://sandratong.com/3.png "掃描Krabby Patty圖卡" )
4. 成功堆疊出要求漢堡則計分、疊錯則重新堆疊
5. 計分與倒數時間顯示於上方
