using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;


namespace Com.Mycompany.burgo
{
    public class launcher : Photon.Pun.MonoBehaviourPunCallbacks
    //繼承Pun已經寫好的一些函式
    {
        #region Private Serializable Fields

        // <summary
        // The maxium number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
        // </summary>

        [Tooltip("The maxium number of players per room. it can't be joined by new players, and so new room will be created.")]
        [SerializeField]
        private byte MaxPlayerPerRoom = 5;

        #endregion

        #region Private Fields

        // <summary>
        // This client's version number. Users are seprated from each other by gameVersion (witch allows you to make breaking changes).
        // </summary>
        string gameVersion = "1";
        bool isConnecting; //為了退出房間後不再被隨意加入房間

        #endregion

        #region MonoBehaviour CallBacks

        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
        /// </summary>

        void Awake()
        {
            /// #Critical
            /// this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all client in the same room sync theit level automaticly.
            /// </summary>
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during initialization phase.
        /// </summary>
        void Start()
        {
            progressLabel.SetActive(false); //一開始為false,輸入玩家名稱並點即進入後為true(顯示正在連線)
            controlPannel.SetActive(true);
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Start the connection process.
        /// - If already connected, we attempt joining a random room.
        /// - If not yet connected, connect this application instance to Photon Cloud Network.
        /// </summary>
        [Tooltip("The UI Pannel to let the user enter name, connect and play")]
        [SerializeField]
        private GameObject controlPannel;
        [Tooltip("The UI Label to inform the user that the connection is in progress")]
        [SerializeField]
        private GameObject progressLabel;


        public void Connect()
        {
            progressLabel.SetActive(true);
            controlPannel.SetActive(false);

            // we check if we are connected or not, we join if we are, else we initiate the connection to the server.
            if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.JoinRandomRoom(); //有連線到，隨機加入房間
            }
            else
            {
                isConnecting = PhotonNetwork.ConnectUsingSettings(); //沒連線到，把現在的狀態設為UsingSetting狀態
                PhotonNetwork.GameVersion = gameVersion;

            }
        }

        #endregion

        #region MonoBehaviourPunCallbacks Callbacks

        public override void OnConnectedToMaster()
        {
            if (isConnecting)
            {
                // #Critical: The first we try to do is to join a potential existing room, If there is, good, else, we'll be called back with OnJoinRandomFailed()
                PhotonNetwork.JoinRandomRoom();
                isConnecting = false;
            }



        }
        public override void OnDisconnected(Photon.Realtime.DisconnectCause cause)
        {
            progressLabel.SetActive(false);
            controlPannel.SetActive(true);

            Debug.LogWarningFormat("PUN Basics Tutorial/Launcher: OnDisconnected() was called by PUN with reson (0)", cause);
        }

        //Konny Changes

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");
            PhotonNetwork.CreateRoom("lobby");
            OnJoinedRoom();
            // PhotonNetwork.LoadLevel("lobby");
        }



        public override void OnJoinedRoom()
        {
            // if (PhotonNetwork.CurrentRoom.PlayerCount == 1) //如果人數是1就載到room1，也就是房長開房
            // {
            //     Debug.Log("We load the 'Room FOR 1' ");

            //     // #Critical
            //     //Load the Room Level.


            // }
            PhotonNetwork.LoadLevel("lobby");
            Debug.Log("PUN Basics Tutorial/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
        }
        #endregion
    }
}