using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class NetworkLauncher : Photon.Pun.MonoBehaviourPunCallbacks
{

    public GameObject loginUI;
    public GameObject nameUI;
    public InputField roomName;
    public InputField playerName;

    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();

    }
    public override void OnConnectedToMaster()//連接到伺服器顯示輸入名稱的UI
    {
        nameUI.SetActive(true);
    }

    // Update is called once per frame
    
}
