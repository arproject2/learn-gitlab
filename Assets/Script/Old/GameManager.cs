﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

using Photon.Pun;
using Photon.Realtime;

namespace Com.Mycompany.burgo
{
    public class GameManager : Photon.Pun.MonoBehaviourPunCallbacks
    {
        public GameObject Player;
        PhotonView PV;
      

        #region Photon Callbacks
        /// <summary>
        /// Called when the local player left the room. we need to load launcher scene.
        /// </summary>
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0); //BackToLaunch
        }

        public override void OnPlayerEnteredRoom(Player other)
        {
            Debug.LogFormat("OnPlayerEnteredRoom() {0}", other.NickName); // not seen if you're the player connecting

            if (PhotonNetwork.IsMasterClient) //FirstOneJoin
            {
                Debug.LogFormat("OnPlayerEnterRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); //called before OnPlayerLeftRoom
                //LoadArena();
            }

        }


        public override void OnPlayerLeftRoom(Player other)
        {
            Debug.LogFormat("OnPlayerLeftRoom() {0}", other.NickName); //seen when other disconnects

            if (PhotonNetwork.IsMasterClient)
            {
                Debug.LogFormat("OnPlayerLeftRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom
                //LoadArena();

            }
        }


        #endregion

        #region Public Methods

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        public void IntoLevel()
        {
            
            PhotonNetwork.LoadLevel("Level1");
            //從這邊改幾個人該加入哪間房
            //PhotonNetwork.LoadLevel("Room" + PhotonNetwork.CurrentRoom.PlayerCount);

        }

        #endregion


        #region Private Methods

        private bool _bMasterCreate = false;
        private bool _bClient = false;

        
        //public Text number01, number02, number03, number04, number05;

        private void Awake()
        {

        }

        //Konmy Changes

        private void Start()
        {
            PV = GetComponent<PhotonView>();
            //if (PV.IsMine && ...)
            //{
            //IntoLevel(); //如果photonview是MasterClient的話，就執行..."點擊加入對應人數房間"
            //}

            if (GameObject.Find("Player(Clone)"))
            {
                 _bMasterCreate = true;
            }

            if (PhotonNetwork.IsMasterClient)
            {
                _bClient = true;
                Debug.Log("IsMasterClient");
            }

            
            if (PhotonNetwork.IsMasterClient) 
            {
                Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount);
                PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "bread"), Vector3.zero, Quaternion.identity);
                foreach (Player player in PhotonNetwork.PlayerList)
                {
                    print("name: " + player.NickName);

                }

            }
            else
            {
                PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "bread2"), Vector3.zero, Quaternion.Euler(new Vector3(0, 0, 5)));
                foreach (Player player in PhotonNetwork.PlayerList)
                {
                    print("name: " + player.NickName);

                }


            }
        }

        void LoadArena() //目前就是不管幾個人都會先進到lobby
        {
            if (!PhotonNetwork.IsMasterClient) 
            {
                Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
            }
            Debug.LogFormat("PhotonNetwork : Loading Level : {0}", PhotonNetwork.CurrentRoom.PlayerCount);
            PhotonNetwork.LoadLevel("lobby");// Loading lobby to contain players first.
                                             // PhotonNetwork.LoadLevel("Room" + PhotonNetwork.CurrentRoom.PlayerCount);// �O�Ъ��A���J�۹����H�Ƴ���
                                             // DontDestroyOnLoad(GameObject.Find("Player(Clone)"));
        }

        #endregion

    }

}