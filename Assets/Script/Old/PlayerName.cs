using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Photon.Pun;
using Photon.Realtime;


namespace Com.Mycompany.burgo
{

    /// <summary>
    ///  Player name input field. Let the user input his name, will appear above the player in the game.
    /// </summary>
    
    [RequireComponent(typeof(InputField))]
    public class PlayerName : MonoBehaviour
    {
        #region Private Constants
        //Store the PlayerPref Key to avoid types
        const string playerNamePreKey = "PlayerName";

        #endregion

        #region MonoBehaviour CallBacks

        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during initialization phase.
        /// </summary>
        
        void Start()
        {

            string defaultName = string.Empty;
            InputField _inputField = GetComponent<InputField>();
            if (_inputField != null)
            {
                if (PlayerPrefs.HasKey(playerNamePreKey))
                {
                    defaultName = PlayerPrefs.GetString(playerNamePreKey);
                    _inputField.text = defaultName;//上次輸入的名稱在下一次會成為預設名稱出現
                }
            }

            PhotonNetwork.NickName = defaultName;

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sets the name of the Player, and save it in PlayerPrefs for future sessions.
        /// </summary>
        /// <param name="value">The name of the Player</param>

        
        public void SetPlayerName(string value) 
        {

            // #Important
            if (string.IsNullOrEmpty(value))
            {

                Debug.LogError("Player Name is null or empty!");
                return;
            }
            PhotonNetwork.NickName = value; //設定玩家所輸入的名稱

            PlayerPrefs.SetString(playerNamePreKey, value);
        }

        #endregion
    }
}
