using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class countdown : MonoBehaviour
{
    public Text time_UI;

    public float now;
    public int remain;
    

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        now = Time.time;
        remain = 180 - Mathf.FloorToInt(now);

        float h = Mathf.FloorToInt(remain / 3600f);
        float m = Mathf.FloorToInt(remain / 60f - h * 60f);
        float s = Mathf.FloorToInt(remain - m * 60f - h * 3600f);

        time_UI.text =  m + ":" + s;
    }
}