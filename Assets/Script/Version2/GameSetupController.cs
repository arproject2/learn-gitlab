using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

using Photon.Pun;


public class GameSetupController : MonoBehaviour
{
   
    void Start()
    {
        CreatePlayer(); //Create a networked player object for each player that loads into the multiplayerScene
    }

  private void CreatePlayer()
    {
        Debug.Log("Creating Players Now!");
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonPlayer"), Vector3.zero, Quaternion.identity);
    }
}
