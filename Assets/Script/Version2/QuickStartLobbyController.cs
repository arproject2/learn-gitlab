using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class QuickStartLobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject quickStartButton; //butt usrd for creating and joining the game.
    [SerializeField]
    private GameObject quickCancelButton;//butt used to stop for game join.
    [SerializeField]
    private int RoomSize;// Manual set number of player in the room at one time.
    
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true; //
        quickStartButton.SetActive(true);
    }

    public void QuickStart()
    {
        quickStartButton.SetActive(false);
        quickCancelButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom(); //first tries to join an existing room.

        Debug.Log("Quick start!");
    }
    public override void OnJoinRandomFailed(short returnCode, string message)//call back
    {
        Debug.Log("Failed to join a room!");
        CreateRoom(); 
    }

    void  CreateRoom() //tring to create our own room
    {
        Debug.Log("Creating room room");
        int randomRoomNumber = Random.Range(0, 100);//creating a random name for the room.
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)RoomSize};
        PhotonNetwork.CreateRoom("Room" + randomRoomNumber, roomOps);//attempting to create a new room.
        Debug.Log(randomRoomNumber);
    }

    public override void OnCreateRoomFailed(short returnCode, string message) //callback function
    {
        Debug.Log("Failed to create room! please try agaiin");
        CreateRoom(); //Retry to create a new room with a different name.
    }

    public void QuickCancel() //Paired to the cancel butt.Used to stop looking for a room to join.
    {
        quickCancelButton.SetActive(false);
        quickCancelButton.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }

}
