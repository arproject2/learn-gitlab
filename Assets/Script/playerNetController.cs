using System;

using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
public class playerNetController : MonoBehaviourPunCallbacks
{
    GameObject gm;
    Button btn;
    Image icon;
    GameObject[] reciptUIs;
    public override void OnLeftRoom()
    {
        //SceneManager.LoadScene(0); //BackToLaunch
    }

    public override void OnEnable()
    {
        base.OnEnable();
        
        //Debug.LogFormat("OnPlayerEnteredRoom() {0}", other.NickName); // not seen if you're the player connecting
        btn = GameObject.FindWithTag("throw").GetComponent<Button>();
        btn.onClick.AddListener(delegate () { sendRpc(); });
        if (PhotonNetwork.IsMasterClient) //FirstOneJoin
        {
            try
            {
                gm = GameObject.FindWithTag("GameController");
                gm.GetComponent<MasterGM>().onStart( updateAllPlayerSprite);
                
            }
            catch
            {
                Debug.Log("feil");
            }
            
            Debug.LogFormat("OnPlayerEnterRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); //called before OnPlayerLeftRoom
                                                                                                   //LoadArena();
        }
        else{
            reciptUIs = GameObject.FindGameObjectsWithTag("recipt");
        }
        
    }
    
    void updateAllPlayerSprite()
    {
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            photonView.RPC("Rpc_SetIcon", RpcTarget.Others, player.ActorNumber, gm.GetComponent<MasterGM>().GetPlayerCurrentFoodIndex(player.ActorNumber));
            Debug.Log("send rpc setIcon");
        }
    }

    public void sendRpc()
    {
        Debug.Log("is mine"+photonView.IsMine);
        if (!photonView.IsMine)
            return;
        if (!PhotonNetwork.IsMasterClient) //FirstOneJoin
        {
            Debug.Log(PhotonNetwork.LocalPlayer.ActorNumber);
            photonView.RPC("Rpc_spawnCube", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber);
        }
        else
        {
            gm.GetComponent<MasterGM>().SpawnCube(PhotonNetwork.LocalPlayer.ActorNumber);
        }
    }
    [PunRPC]
    void Rpc_spawnCube(int playerIndex) 
    {
        
        gm = GameObject.FindWithTag("GameController");
        Debug.Log("get player"+ playerIndex);
        gm.GetComponent<MasterGM>().SpawnCube(playerIndex);    
    }
    [PunRPC]
    void Rpc_SetIcon(int playerIndex,int foodIndex)
    {
    
        if (playerIndex == PhotonNetwork.LocalPlayer.ActorNumber)
        {
            Debug.Log("recive:" + playerIndex + "food:" + foodIndex);
            gm = GameObject.FindWithTag("GameController");
            icon = GameObject.FindGameObjectWithTag("foodSlot").GetComponent<Image>();
            icon.sprite = gm.GetComponent<MasterGM>().GetFoodIcon(foodIndex);
        }
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        if(PhotonNetwork.IsMasterClient||changedProps==null) return;
        
        if(changedProps.ContainsKey("receipt")){
            
            string[] receipt = changedProps["receipt"].ToString().Split(',');
            for(int i=0;i<reciptUIs.Length;i++){
                int index = Int16.Parse( reciptUIs[i].name.Split(' ')[1])-1;
                Debug.Log(index);
                try{
                reciptUIs[i].GetComponent<Image>().sprite 
                    = gm.GetComponent<MasterGM>().GetFoodIcon(Int16.Parse(receipt[index]));
                }catch{

                }
            }
            
        }
        if(changedProps.ContainsKey("starttime")){
            gm.GetComponent<MasterGM>().setStartTime(Int32.Parse(changedProps["starttime"].ToString()));
        }
        if(changedProps.ContainsKey("point")){
            GameObject.FindGameObjectWithTag("point").GetComponent<Text>().text = changedProps["point"].ToString();
        }
    }



}
