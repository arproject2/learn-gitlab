using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class button : MonoBehaviour
{
    public GameObject[] fab;
    public Transform spawnpoint;
    [SerializeField] private int currentFoodIndex=-1;
    public Text text;

    private int score;

    // Start is called before the first frame update
    void Start()
    {
        score += 1;
        text.text = score.ToString();

        if (currentFoodIndex == -1)
        {
            currentFoodIndex = Random.Range(0, fab.Length - 1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (currentFoodIndex == -1)
        {
            currentFoodIndex = Random.Range(0, fab.Length-1);
        }
        */
    }
    public void SpawnCube(int currentFoodIndex)
    {
        if (currentFoodIndex != -1)
        {
            Instantiate(fab[currentFoodIndex], spawnpoint.position, fab[currentFoodIndex].transform.rotation);
            currentFoodIndex = -1;
        }
    }
}
