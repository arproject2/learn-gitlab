using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    [SerializeField]private int foodType=0;//cheese=1,
    [SerializeField]private Sprite foodIcon;
    [SerializeField]private string foodName;

    
    public int GetFoodType() {
        return this.foodType;
    }
    public Sprite GetIcon(){
        return this.foodIcon;
    }
    public string GetFoodName(){
        return this.foodName;
    }
    public void DestoryMySelf(){
        Destroy(this.gameObject);
    }
}
