using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;


public class NetworkController : MonoBehaviourPunCallbacks
{
   
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();//Connects to Photon master servers
        
    }


    public override void OnConnectedToMaster()
    {
        Debug.Log("We are now connected to the " + PhotonNetwork.CloudRegion + " server!");
    }
}
