using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Photon.Pun;
using Photon.Realtime;


public class CustomMatchmakingLobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    public GameObject lobbyConnectButton; //button used for joining a lobby.
    [SerializeField]
    private GameObject lobbyPanel; //panel for displaying lobby.
    [SerializeField]
    private GameObject mainPanel; //panel for displaying the main menu.
    [SerializeField]
    private InputField playerNameInput; //Input filed so player can change their NickName
    

    private string roomName; //string for saving room name
    private int roomSize; //int for saving room size

    private List<RoomInfo> roomListings; //list of current rooms
    [SerializeField]
    private Transform roomsContainer; //container for holding all the room listings
    [SerializeField]
    private GameObject roomListingPrefab;//prefab for displayer each room in the lobby

    public override void OnConnectedToMaster() //callback function for when the first connection is esist
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        lobbyConnectButton.SetActive(true);
        roomListings = new List<RoomInfo>();//initializing roomlisting

        //check for player name to player prefs
        if (PlayerPrefs.HasKey("NickName"))
        {
            if (PlayerPrefs.GetString("NickName") == "")
            {
                PhotonNetwork.NickName = "Player " + Random.Range(0, 100); //random player name when it's empty
            }
            else
            {
                PhotonNetwork.NickName = PlayerPrefs.GetString("NickName");//to get saved name
            }

        }
        else
        {
            PhotonNetwork.NickName = "Player " + Random.Range(0, 100);//update input field with player name
        }
        playerNameInput.text = PhotonNetwork.NickName; //update input filed with player name
    }

    public void PlayeNameUpdate(string nameInput) //Input fuction foor player name.
    {
        PhotonNetwork.NickName = nameInput;
        PlayerPrefs.SetString("NickName", nameInput);
        playerNameInput.text = nameInput;
    }

    public void JoinLobbyOnClick() //paired to the delay start button
    {
        mainPanel.SetActive(false);
        lobbyPanel.SetActive(true);
        PhotonNetwork.JoinLobby(); //first tries to join an existing room.
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        int tempIndex;
        foreach (RoomInfo room in roomList) //loop through each room in room list
        {
            if (roomListings != null)//try to find existing room listing
            {
                tempIndex = roomListings.FindIndex(ByName(room.Name));
            }
            else
            {
                tempIndex = -1;
            }
            if (tempIndex != -1) //remove listing because it has been closed
            {
                roomListings.RemoveAt(tempIndex);
                Destroy(roomsContainer.GetChild(tempIndex).gameObject);

            }
            if (room.PlayerCount > 0) //add room listing because it is new
            {
                roomListings.Add(room);
                ListRoom(room);
            }

        }
    }
    static System.Predicate<RoomInfo> ByName(string name)//predicate function for seach through room
    {
        return delegate (RoomInfo room)
        {
            return room.Name == name;

        };
    }

    void ListRoom(RoomInfo room)
    {
        if (room.IsOpen && room.IsVisible)
        {
            GameObject tempListing = Instantiate(roomListingPrefab, roomsContainer);
            RoomButton tempButton = tempListing.GetComponent<RoomButton>();
            tempButton.SetRoom(room.Name, room.MaxPlayers, room.PlayerCount);

        }
    }

    public void OnRoomNameChanged(string nameIn) //
    {
        roomName = nameIn;

    }
    public void OnRoomSizeChanged(string sizeIn)
    {
        roomSize = int.Parse(sizeIn);
    }
    public void CreateRoom() //function paired to the create room button
    {
        Debug.Log("Creating room now");
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, MaxPlayers = (byte)roomSize };
        PhotonNetwork.CreateRoom(roomName, roomOps);//attempting to create a new room
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to create a new room but failed, there must already be a room with same name!");
    }

    public void MatchmakingCancel() //Paired to the cancelbutton. Used to go back the menu
    {
        mainPanel.SetActive(true);
        lobbyPanel.SetActive(false);
        PhotonNetwork.LeaveLobby();
    }
}




