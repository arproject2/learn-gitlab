using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RandomPair : MonoBehaviour
{
    
    [SerializeField]int playerAmt;

    [SerializeField] int numberOfRecipe; //漢堡層數
    [SerializeField]int[] foodSortList; //食材包
    [SerializeField]int[] recipe; //官方指定菜單
    
    [SerializeField]GameObject[] fab; 
    
    
    [SerializeField]float gameDuration;
    [SerializeField]int userId=0;
    [SerializeField]int point;
    
    [SerializeField]Transform spawnpoint;
    [SerializeField]Text timeText;
    [SerializeField]Text pointText;
    [SerializeField]Image foodIcon;
    [SerializeField] GameObject resetButton;
    //private 
    int indexOfCurrentRecipe;
    int valueOfCurrentRecipe;
    float startTime;
    Food[] foodList;
    int[] tmpList;
    // Start is called before the first frame update
    void Start()
    {

        LoadFoodList();
        RestartGame();
       
    }
    
    // Update is called once per frame
    void Update()
    {
        //if is out of time
        timeText.text =((int)(startTime+gameDuration-Time.time)).ToString();
        if (Time.time >= startTime+gameDuration  )
        {
            GameOver();
        }
        
    }
    IEnumerator waitforInit(float sec){

        //生成漢堡上蓋可以寫在這裡

        yield return new WaitForSeconds(sec);
        InitGameState();//計分完後刷新桌面
        

        
    }
    
    public void SpawnCube(int playerIndex)
    {
        int currFoodValue = foodSortList[playerIndex];
        //condiction of the recipit
        if (currFoodValue == recipe[indexOfCurrentRecipe]) //如果陣列編號的食材模型=現在菜單要求食材的編號
        {
            Instantiate(fab[currFoodValue], spawnpoint.position, fab[currFoodValue].transform.rotation); //就生成陣列編號對應模型
            //++ == +=1
            //   == +1
            indexOfCurrentRecipe++;
            CheckWinCondition();
        }
        else
        {
            InitGameState(); //如果編號沒對上、就呼叫InitGameState()、刪除食物模型並重新random菜單
        }
    }
    void CheckWinCondition(){
        if(indexOfCurrentRecipe>=recipe.Length){
            GetOnePoint();
        }
        else
        {
            RandomizeIntArray(tmpList);
            DrawFoodFromFab();
        }
    }
    public void GetOnePoint(){
        point++;
        pointText.text = point.ToString();
        this.StartCoroutine("waitforInit",2f);
        
    }
    public void GameOver()
    {
        Debug.Log("gameOver");
        //遊戲結束後
        resetButton.SetActive(true);
        //顯示restartButton
    }
    
    public void InitGameState()
    {
        indexOfCurrentRecipe = 0;
        DeleteAllInstantedFood();
        RandomizeIntArray(tmpList);
        DrawFoodFromFab();
     

        foodIcon.sprite = foodList[foodSortList[userId]].GetIcon();
        
    }
    public void RestartGame()
    {
        point = 0;
        pointText.text = point.ToString();
        InitGameState();
        startTime = Time.time;
        resetButton.SetActive(false);
        //start game 之後把restartButton 隱藏
    }
    private void DeleteAllInstantedFood()
    {
        GameObject[] arrayOfInstantedFood = GameObject.FindGameObjectsWithTag("InstantedFood");
        
        /*
        for(int i = 0; i < arrayOfInstantedFood.Length; i++)
        {
            Destroy(arrayOfInstantedFood[i]);
        }
        */
        foreach(GameObject obj in arrayOfInstantedFood)
        {
            Debug.Log(obj.name);
            obj.GetComponent<Food>().DestoryMySelf();
            
        }
    }
    void LoadFoodList(){
        //load food typeId
        foodList = new Food[fab.Length];
        foodSortList = new int[playerAmt];//原料長度=玩家數
        recipe = new int[numberOfRecipe];//菜單長度
        tmpList = new int[fab.Length];
        for(int i=0;i<fab.Length;i++){
            Food currFood = fab[i].GetComponent<Food>();
            foodList[i] = currFood;
            tmpList[i] = currFood.GetFoodType();
        }
        
        
        
    }
    public void RandomizeIntArray(int[] list)
    {
        int rnd,tmp;
        
        for(int j=0;j<2;j++){
            for (int i = 0; i < list.Length; i++)
            {
                rnd = Random.Range(0, list.Length);
                tmp = list[rnd];
                list[rnd] = list[i];
                list[i] = tmp;
            }
        }
    }
    private void DrawFoodFromFab(){
        
        
        for(int i=0;i<recipe.Length;i++){
            recipe[i] = tmpList[i];
            foodSortList[i] = tmpList[i];
        }
        valueOfCurrentRecipe = Random.Range(0, foodSortList.Length);
        recipe[indexOfCurrentRecipe] = foodSortList[valueOfCurrentRecipe];
    }
    /*
     * todo:
     *  food ui
     *  //gameover
     *  //score
     *  //ramdoms
     *  start and end scance
     *  network
     */
}
