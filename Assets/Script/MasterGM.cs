using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Pun;
using ExitGames.Client.Photon;
public class MasterGM : MonoBehaviour
{


    [SerializeField] int playerAmt;

    [SerializeField] int numberOfRecipe; //�~���h��
    [SerializeField] int[,] foodSortList; //�����]
    [SerializeField] int[] recipe; //�x����w���

    [SerializeField] GameObject[] fab;
    [SerializeField] GameObject breadup, breaddown;

    [SerializeField] float gameDuration;
    //[SerializeField] int userId = 0;
    [SerializeField] int point;

    [SerializeField] Transform spawnpoint;
    [SerializeField] Text timeText;
    [SerializeField] Text pointText;
    [SerializeField] Image foodIcon;
    [SerializeField] Image[] recipeIcon;

    [SerializeField] GameObject resetButton;
    //private 
    int indexOfCurrentRecipe;
    int valueOfCurrentRecipe;
    float startTime;
    Food[] foodList;
    int[] tmpList;
    public delegate void updateAllPlayerSprite();
    updateAllPlayerSprite updateicon;
    // Start is called before the first frame update
    private void Start() {
        ExitGames.Client.Photon.Hashtable props =new ExitGames.Client.Photon.Hashtable()
        {
            
            {"point",point}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
        
    }
    public void onStart(updateAllPlayerSprite update)
    {
        updateicon = update;
        playerAmt = GameObject.FindGameObjectsWithTag("Player").Length;
        LoadFoodList();
        RestartGame();

    }

    // Update is called once per frame
    public void Update()
    {
        
        int nowtime = PhotonNetwork.ServerTimestamp/1000; 
        //if is out of time
        timeText.text = ((int)(startTime + gameDuration - nowtime)).ToString();
        if (nowtime >= startTime + gameDuration)
        {
            GameOver();
        }

    }
    IEnumerator waitforInit(float sec)
    {

        //�ͦ��~���W�\�i�H�g�b�o��
        yield return new WaitForSeconds(0.5f);
        GameObject OBJ = PhotonNetwork.Instantiate(breadup.name, spawnpoint.position, breadup.transform.rotation);
        
        OBJ.SetActive(true);
        yield return new WaitForSeconds(sec);
        InitGameState();//�p�������s�ୱ



    }
    IEnumerator waitforSendUpdateRecipt()
    {

        
        yield return new WaitForSeconds(0.5f);
        ExitGames.Client.Photon.Hashtable props =new ExitGames.Client.Photon.Hashtable()
        {
            {"receipt", string.Join(",",recipe)}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);



    }
    public void SpawnCube(int playerIndex)
    {
        int currFoodValue = foodSortList[playerIndex - 1,indexOfCurrentRecipe];
        //condiction of the recipit
        if (currFoodValue == recipe[indexOfCurrentRecipe]) //�p�G�}�C�s���������ҫ�=�{�b���n�D�������s��
        {
            PhotonNetwork.Instantiate(fab[currFoodValue].name, spawnpoint.position, fab[currFoodValue].transform.rotation); //�N�ͦ��}�C�s�������ҫ�
            //++ == +=1
            //   == +1
            indexOfCurrentRecipe++;
            CheckWinCondition();
        }
        else
        {
            InitGameState(); //�p�G�s���S��W�B�N�I�sInitGameState()�B�R�������ҫ��í��srandom���
        }
    }
    void CheckWinCondition()
    {
        if (indexOfCurrentRecipe >= recipe.Length)
        {
            GetOnePoint();
        }
        else
        {
            RandomizeIntArray(tmpList);
            UpdateFoodIcon();
            //DrawFoodFromFab();
        }
    }
    public void GetOnePoint()
    {
        
        point++;
        pointText.text = point.ToString();
        synPoint();
        this.StartCoroutine("waitforInit", 2f);

    }
    void synPoint(){
        ExitGames.Client.Photon.Hashtable props =new ExitGames.Client.Photon.Hashtable()
        {
            {"point",point}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
    }
    public void GameOver()
    {
        Debug.Log("gameOver");
        //�C��������
        //resetButton.SetActive(true);
        //���restartButton
    }

    public void InitGameState()
    {
        indexOfCurrentRecipe = 0;
        DeleteAllInstantedFood();
        //RandomizeIntArray(tmpList);
        DrawFoodFromFab();
        GameObject OBJ = PhotonNetwork.Instantiate(breaddown.name, spawnpoint.position, breaddown.transform.rotation);
        OBJ.SetActive(true);
        UpdateReceiptToUI();
        UpdateFoodIcon();
        //send to all


    }
    private void UpdateReceiptToUI(){
        recipeIcon[indexOfCurrentRecipe].sprite = foodList[foodSortList[valueOfCurrentRecipe,indexOfCurrentRecipe]].GetIcon();
        
        for(int i =0;i< numberOfRecipe;i++){
            recipeIcon[i].sprite = foodList[recipe[i]].GetIcon();
        }
        StartCoroutine("waitforSendUpdateRecipt");
    }
    public void UpdateFoodIcon(){
        updateicon();
        foodIcon.sprite = foodList[foodSortList[PhotonNetwork.LocalPlayer.ActorNumber-1,indexOfCurrentRecipe]].GetIcon();
    }
    public void RestartGame()
    {
        point = 0;
        pointText.text = point.ToString();
        synPoint();
        InitGameState();
        startTime = PhotonNetwork.ServerTimestamp/1000;
        ExitGames.Client.Photon.Hashtable props =new ExitGames.Client.Photon.Hashtable()
        {
            {"starttime",startTime.ToString()}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
        //resetButton.SetActive(false);
        //start game �����restartButton ����
    }
    private void DeleteAllInstantedFood()
    {
        GameObject[] arrayOfInstantedFood = GameObject.FindGameObjectsWithTag("InstantedFood");

        /*
        for(int i = 0; i < arrayOfInstantedFood.Length; i++)
        {
            Destroy(arrayOfInstantedFood[i]);
        }
        */
        foreach (GameObject obj in arrayOfInstantedFood)
        {
            Debug.Log(obj.name);
            PhotonNetwork.Destroy(obj);

        }
    }
    public void LoadFoodList()
    {
        //load food typeId
        foodList = new Food[fab.Length];
        foodSortList = new int[playerAmt,numberOfRecipe];//��ƪ���=���a��
        recipe = new int[numberOfRecipe];//������
        tmpList = new int[fab.Length];
        for (int i = 0; i < fab.Length; i++)
        {
            Food currFood = fab[i].GetComponent<Food>();
            foodList[i] = currFood;
            tmpList[i] = currFood.GetFoodType();
        }



    }
    public void RandomizeIntArray(int[] list)
    {
        int rnd, tmp;

        for (int j = 0; j < 2; j++)
        {
            for (int i = 0; i < list.Length; i++)
            {
                rnd = Random.Range(0, list.Length);
                tmp = list[rnd];
                list[rnd] = list[i];
                list[i] = tmp;
            }
        }
    }
    private void DrawFoodFromFab()
    {
        
        for (int i = 0; i < recipe.Length; i++)
        {
            RandomizeIntArray(tmpList);
            for(int playerIndex=0;playerIndex<playerAmt;playerIndex++)
                foodSortList[playerIndex,i] = tmpList[playerIndex];
            recipe[i] = tmpList[i];
            valueOfCurrentRecipe = Random.Range(0, playerAmt);
            recipe[i] = foodSortList[valueOfCurrentRecipe,i];
            
        }
        //valueOfCurrentRecipe = Random.Range(0, playerAmt);
        //recipe[indexOfCurrentRecipe] = foodSortList[valueOfCurrentRecipe];
        //recipeIcon[indexOfCurrentRecipe].sprite = foodList[foodSortList[valueOfCurrentRecipe]].GetIcon();

        //updateicon();
        //foodIcon.sprite = foodList[foodSortList[PhotonNetwork.LocalPlayer.ActorNumber-1]].GetIcon();
        //foodIcon.sprite = foodList[foodSortList[userId]].GetIcon();
        
    }
    public Sprite GetFoodIcon(int index)
    {
        return fab[index].GetComponent<Food>().GetIcon();
    }
    public int GetPlayerCurrentFoodIndex(int player)
    {
        return foodSortList[player - 1,indexOfCurrentRecipe];
    }
    public void setStartTime(int starttime){
        this.startTime = starttime;
    }
   
}
